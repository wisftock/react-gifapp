import useFecthGifs from '../../hooks/useFecthGifs';
import { renderHook } from '@testing-library/react-hooks';
describe('Prueba en useFetchGifs', () => {
  test('debe de retornar el estado inicial', async () => {
    // const { data, loading } = useFecthGifs('naruto');
    // crea compomente virtual
    const { result, waitForNextUpdate } = renderHook(() =>
      useFecthGifs('naruto')
    );
    const { data, loading } = result.current;
    await waitForNextUpdate();

    expect(data).toEqual([]);
    expect(loading).toBe(true);
    // console.log(data, loading);
  });
  test('debe de retornar un arreglo de imgs y el loading en false', async () => {
    //   regresa promesa cuando sucedio un cambio en el hook
    const { result, waitForNextUpdate } = renderHook(() =>
      useFecthGifs('naruto')
    );
    await waitForNextUpdate();
    const { data, loading } = result.current;
    expect(data.length).toBe(15);
    expect(loading).toBe(false);
  });
});
