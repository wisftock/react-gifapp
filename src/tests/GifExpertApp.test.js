import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import GifExpertApp from '../GifExpertApp';

describe('Prueba en <GifExpertApp/>', () => {
  test('debe de mostrar GifExpertApp correctamente', () => {
    const wrapper = shallow(<GifExpertApp />);
    expect(wrapper).toMatchSnapshot();
  });
  test('debe mostrar una lista de categorias', () => {
    const categorias = ['goku', 'naruto'];
    const wrapper = shallow(<GifExpertApp defaultCategory={categorias} />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('GifGrid').length).toBe(categorias.length);
  });
});
