import { shallow } from 'enzyme';
import GifGridItem from '../../components/GifGridItem';

describe('Prueba en GifGridItem', () => {
  const title = 'Un titulo';
  const url = 'https://url';
  const wrapper = shallow(<GifGridItem title={title} url={url} />);
  test('debe de mostrar correctamente', () => {
    expect(wrapper).toMatchSnapshot();
  });
  test('debe de tener un parrafo con el titulo', () => {
    const p = wrapper.find('p');
    expect(p.text().trim()).toBe(title);
  });
  test('debe de tener un url y alt de los props', () => {
    const img = wrapper.find('img');
    expect(img.prop('src')).toBe(url);
    expect(img.prop('alt')).toBe(title);
  });
  test('debe de tener el animate_fadeIn', () => {
    const div = wrapper.find('div').at(0);
    // console.log(div.prop('className'));
    // validando si en el div hay dicha clase
    expect(div.hasClass('card')).toBe(true);
    expect(div.prop('className')).toBe('card');
    expect(div.prop('className').includes('card')).toBe(true);
  });
});
