import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import GifGrid from '../../components/GifGrid';
import useFecthGifs from '../../hooks/useFecthGifs';
jest.mock('../../hooks/useFecthGifs');
describe('Prueba en <GifGrid />', () => {
  const category = 'naruto';

  test('debe mostrarse correctamente', () => {
    useFecthGifs.mockReturnValue({
      data: [],
      loading: true,
    });
    const wrapper = shallow(<GifGrid category={category} />);
    expect(wrapper).toMatchSnapshot();
  });

  test('debe de mostrar items cuando se cargan imagenes useFetchGifs', () => {
    const gifs = [
      {
        id: 'ABC',
        url: 'https://url.png',
        title: 'Cualquier cosa',
      },
      {
        id: '123',
        url: 'https://url.png',
        title: 'Cualquier cosa',
      },
    ];
    useFecthGifs.mockReturnValue({
      data: gifs,
      loading: false,
    });
    // fingir que tenga informacion mock
    const wrapper = shallow(<GifGrid category={category} />);
    // el parrafo no debe de existir
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('p').exists()).toBe(false);

    // debe de tener el mismo length
    expect(wrapper.find('GifGridItem').length).toBe(gifs.length);
  });
});
