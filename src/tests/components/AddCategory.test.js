import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import AddCategory from '../../components/AddCategory';

describe('Prueba en <AddCategory/>', () => {
  const setCategories = jest.fn();
  let wrapper = shallow(<AddCategory setCategories={setCategories} />);
  // antes de todas
  beforeEach(() => {
    // para limpiar
    jest.clearAllMocks();
    wrapper = shallow(<AddCategory setCategories={setCategories} />);
  });
  test('debe mostrarse correctamente', () => {
    expect(wrapper).toMatchSnapshot();
  });

  test('debe de cambiar la caja de texto', () => {
    const input = wrapper.find('input');
    const value = 'Hola Mundo';

    input.simulate('change', { target: { value } });
    expect(wrapper.find('p').text().trim()).toBe(value);
  });

  test('debe dar funcionalidad al input', () => {
    const input = wrapper.find('input');
    const value = 'Hola Mundo';

    input.simulate('change', { target: { value } });
    expect(wrapper.find('p').text().trim()).toBe(value);
  });
  test('no debe de postear la informacion con submit', () => {
    wrapper.find('form').simulate('submit', { preventDefault() {} });
    // que se haya llamado el submit (not se niega)
    expect(setCategories).not.toHaveBeenCalled();
  });
  test('debe de llamar el setCategory y limpiar la caja de texto', () => {
    const value = 'Hola Mundo';

    wrapper.find('input').simulate('change', { target: { value } });
    wrapper.find('form').simulate('submit', { preventDefault() {} });

    // que se llame una vez
    expect(setCategories).toHaveBeenCalled();
    // que se llame dos veces
    expect(setCategories).toHaveBeenCalledTimes(1);
    // que se pase la funcion
    expect(setCategories).toHaveBeenCalledWith(expect.any(Function));

    // el input debe estar en ''
    expect(wrapper.find('input').prop('value')).toBe('');
  });
});
