import getGifs from '../../helpers/getGifs';

describe('Prueba en <getGifs/>', () => {
  test('debe de traer 15 elementos', async () => {
    const gifs = await getGifs('goku');
    expect(gifs.length).toBe(15);
    // console.log(gifs);
  });
  test('debe verificar si no se manda argumentos', async () => {
    const gifs = await getGifs('');
    expect(gifs.length).toBe(0);
    // console.log(gifs);
  });
});
