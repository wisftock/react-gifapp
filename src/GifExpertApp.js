import React, { useState } from 'react';
import AddCategory from './components/AddCategory';
import GifGrid from './components/GifGrid';

const GifExpertApp = ({ defaultCategory = [] }) => {
  const [categories, setCategories] = useState(defaultCategory);

  return (
    <div className='container-fluid contenedor text-white'>
      <div className='row text-center p-3 col-11 m-auto'>
        <h2>GifExpertApp</h2>
      </div>
      <AddCategory setCategories={setCategories} />
      <hr />

      <div className='row'>
        {categories.map((category) => {
          return <GifGrid key={category} category={category} />;
        })}
      </div>
    </div>
  );
};

export default GifExpertApp;
