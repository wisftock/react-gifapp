import React from 'react';
import useFecthGifs from '../hooks/useFecthGifs';
import GifGridItem from './GifGridItem';
import PropTypes from 'prop-types';

const GifGrid = ({ category }) => {
  const { data: images, loading } = useFecthGifs(category);

  return (
    <>
      <h3 className='animate__animated animate__bounce'>{category}</h3>
      {loading && (
        <p className='animate__animated animate__flash'>Loading...</p>
      )}
      <div className='d-flex flex-wrap justify-content-around animate__animated animate__fadeIn'>
        {images.map((img) => {
          return <GifGridItem key={img.id} {...img} />;
        })}
      </div>
    </>
  );
};
GifGrid.propTypes = {
  category: PropTypes.string.isRequired,
};
export default GifGrid;
